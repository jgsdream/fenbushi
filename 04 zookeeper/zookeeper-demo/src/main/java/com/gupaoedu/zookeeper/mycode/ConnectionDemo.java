package com.gupaoedu.zookeeper.mycode;

import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;

/**
 * @Description 连接服务端
 * @Author XIONGCAIZHAI
 * @Date 2019/10/31 17:25
 **/
public class ConnectionDemo{
    
    public static void main(String[] args){
        try{
            ZooKeeper zooKeeper = new ZooKeeper("192.168.187.128,192.168.187.129,192.168.187.130",4000,null);
            System.out.println(zooKeeper.getState());       //CONNECTING
    
            //Thread.sleep(1000);
    
            System.out.println(zooKeeper.getState());   //CONNECTED
            
            
            zooKeeper.close();
        }catch(IOException e){
            e.printStackTrace();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    
    
    }
}
