package com.gupaoedu.rmi.rpc;

import com.gupaoedu.rmi.rpc.zk.IRegisterCenter;
import com.gupaoedu.rmi.rpc.zk.RegisterCenterImpl;

import java.io.IOException;

/**
 * 腾讯课堂搜索 咕泡学院
 * 加群获取视频：608583947
 * 风骚的Michael 老师
 */
/* @Point RPC+注册中心
 * @Analyse  包含版本号version、负载均衡
 * @Date     2019/11/4 10:04
 **/
public class ServerDemo {
    public static void main(String[] args) throws IOException {
        IGpHello iGpHello=new GpHelloImpl();    //80node
        IGpHello iGpHello1=new GpHelloImpl2();  //81node
        IRegisterCenter registerCenter=new RegisterCenterImpl();

        RpcServer rpcServer=new RpcServer(registerCenter,"127.0.0.1:8080");
        rpcServer.bind(iGpHello,iGpHello1);
        rpcServer.publisher();
        System.in.read();
    }
}
