package com.gupaoedu.rmi.rpc.zk;

/**
 * 腾讯课堂搜索 咕泡学院
 * 加群获取视频：608583947
 * 风骚的Michael 老师
 */
/* @Point 注册中心
 * @Analyse  TODO
 * @Date     2019/11/4 10:24
 **/
public interface IRegisterCenter {

    /**
     * 注册服务名称和服务地址
     * @param serviceName
     * @param serviceAddress
     */
    void register(String serviceName,String serviceAddress);
}
