package com.gupaoedu.zookeeper;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;

/**
 * Curator封装了zookeeper原生API。
 * 伪代码，没有实现功能
 * 三行代码实现了分布式锁？
 * 还可以做leader选举等等
 * （这里我没深入学）
 */
public class CuratorDemo {
    public static void main(String[] args) {
        CuratorFramework curatorFramework=CuratorFrameworkFactory.builder().build();
        InterProcessMutex interProcessMutex=new InterProcessMutex(curatorFramework,"/locks");  //InterProcessMutex这里点进去，可以看到源码用了可重入锁
        try {
            interProcessMutex.acquire();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
