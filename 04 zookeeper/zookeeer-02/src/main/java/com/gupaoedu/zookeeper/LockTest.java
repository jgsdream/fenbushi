package com.gupaoedu.zookeeper;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 *  DistributedLock没有做连接处理，第一次启动可能会报错
 *  连接成功之后，在linux中删除当前锁节点，触发监听，使得下一个节点获得锁，如delete /locks/0000000010， 0000000011会获得锁
 *  学员讲：100个人抢10个秒杀可以用这个代码
 *  这个功能的代码太多了，所以引入Curator简化代码。Curator封装了zookeeper原生API。
 */
public class LockTest
{
    public static void main( String[] args ) throws IOException {
        CountDownLatch countDownLatch=new CountDownLatch(10);
        for(int i=0;i<10;i++){
            new Thread(()->{
                try {
                    countDownLatch.await();
                    DistributedLock distributedLock=new DistributedLock();
                    distributedLock.lock(); //获得锁
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },"Thread-"+i).start();
            countDownLatch.countDown();
        }
        System.in.read();
    }
}
