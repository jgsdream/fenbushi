package com.gupaoedu.zookeeper.mycode;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @Description 分布式锁：后来的节点监听它的前一个一节
 * @Author XIONGCAIZHAI
 * @Date 2019/11/1 10:51
 **/
public class DistributedLock implements Lock, Watcher{
    
    
    @Override
    public void lock(){
    
    }
    
    @Override
    public void lockInterruptibly() throws InterruptedException{
    
    }
    
    @Override
    public boolean tryLock(){
        return false;
    }
    
    @Override
    public boolean tryLock(long time,TimeUnit unit) throws InterruptedException{
        return false;
    }
    
    @Override
    public void unlock(){
    
    }
    
    @Override
    public Condition newCondition(){
        return null;
    }
    
    @Override
    public void process(WatchedEvent event){
    
    }
}
