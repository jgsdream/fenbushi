package com.gupao.dubbo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @Description 通过spring加载配置的方式，启动dubbo
 * @Author XIONGCAIZHAI
 * @Date 2019/11/5 10:25
 **/
public class Bootstrap{
    
    public static void main(String[] args){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("META-INF/spring/dubbo-server.xml");
        context.start();
        
        try{
            System.in.read(); //阻塞当前线程
        }catch(IOException e){
            e.printStackTrace();
        }
    
    }
}
