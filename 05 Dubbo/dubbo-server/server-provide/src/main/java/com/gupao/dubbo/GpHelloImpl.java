package com.gupao.dubbo;

/**
 * @Description TODO
 * @Author XIONGCAIZHAI
 * @Date 2019/11/5 9:21
 **/
public class GpHelloImpl implements IGpHello{
    
    @Override
    public String sayHello(String msg){
        return "Hello:" + msg;
    }
}
