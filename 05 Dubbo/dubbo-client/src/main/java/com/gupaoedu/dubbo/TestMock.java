package com.gupaoedu.dubbo;

import com.gupao.dubbo.IGpHello;

/**
 * @Description TODO
 * @Author XIONGCAIZHAI
 * @Date 2019/11/11 19:43
 **/
public class TestMock implements IGpHello{
    @Override
    public String sayHello(String s){
        return "系统繁忙：" + s;
    }
}
