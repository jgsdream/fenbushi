package com.gupaoedu.dubbo;

import com.alibaba.dubbo.common.extension.ExtensionLoader;
import com.alibaba.dubbo.rpc.Protocol;
import com.gupao.dubbo.IGpHello;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Description TODO
 * @Author XIONGCAIZHAI
 * @Date 2019/11/5 9:01
 **/
public class DemoTest{
    public static void main(String[] args){
        /*这段代码让我想明白了@Override及以下的这段代码：IGpHello有个接口，没有实现，所以强制生成了@Override及以下的这段代码，交由这里去实现
        IGpHello iGpHello= new IGpHello(){
            @Override
            public String sayHello(String s){
                return null;
            }
        };*/
    
        //iGpHello 没有实现，如何获得它的实现呢？借助Dubbo，通过发布服务的方式
        /*IGpHello iGpHello = null;
        System.out.println(iGpHello.sayHello("Mic"));*/
    
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("dubbo-client.xml");
    
        
        IGpHello iGpHello = (IGpHello)context.getBean("gpHelloService");
        System.out.println(iGpHello.sayHello("Mic1"));
    
    
        //协议扩展（抛转引玉）
        /*Protocol protocol = ExtensionLoader.getExtensionLoader(Protocol.class)
                                      .getExtension("myProtocol");
        System.out.println(protocol.getDefaultPort());*/
    
        //源码入口
        Protocol protocol1 = ExtensionLoader.getExtensionLoader(Protocol.class)
                                    .getDefaultExtension();
        System.out.println(protocol1.getDefaultPort());
    }
}
