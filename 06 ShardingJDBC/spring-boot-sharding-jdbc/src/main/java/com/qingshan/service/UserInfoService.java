package com.qingshan.service;

/**
 * @author qingshan
 */
public interface UserInfoService {

    void insertBatchUserInfo();

    void queryUserIfo(Long uid);

    void testTransactional();
}
