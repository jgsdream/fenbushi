package com.qingshan.service;

/**
 * @author qingshan
 */
public interface OrderService {
    void queryOrderById(Long orderId);

    void insertBatchOrder();
}
