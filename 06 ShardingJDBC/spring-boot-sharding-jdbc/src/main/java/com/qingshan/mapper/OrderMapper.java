package com.qingshan.mapper;

import com.qingshan.entity.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author qingshan
 */
@Mapper
public interface OrderMapper {

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Long orderId);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);
}
