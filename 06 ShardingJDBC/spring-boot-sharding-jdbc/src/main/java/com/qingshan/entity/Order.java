package com.qingshan.entity;

import lombok.Data;

/**
 * @author qingshan
 */
@Data
public class Order {

    private Long orderId;

    private Long userId;
}
