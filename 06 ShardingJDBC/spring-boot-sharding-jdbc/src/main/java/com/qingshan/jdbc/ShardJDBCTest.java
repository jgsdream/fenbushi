package com.qingshan.jdbc;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.shardingsphere.api.config.sharding.ShardingRuleConfiguration;
import org.apache.shardingsphere.api.config.sharding.TableRuleConfiguration;
import org.apache.shardingsphere.api.config.sharding.strategy.InlineShardingStrategyConfiguration;
import org.apache.shardingsphere.shardingjdbc.api.ShardingDataSourceFactory;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @Author: qingshan
 */
public class ShardJDBCTest {
    public static void main(String[] args) throws SQLException {
        // 配置真实数据源
        Map<String, DataSource> dataSourceMap = new HashMap<>();

        // 配置第一个数据源
        DruidDataSource dataSource1 = new DruidDataSource();
        dataSource1.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource1.setUrl("jdbc:mysql://127.0.0.1:3306/ds0");
        dataSource1.setUsername("root");
        dataSource1.setPassword("root");
        dataSourceMap.put("ds0", dataSource1);

        // 配置第二个数据源
        DruidDataSource dataSource2 = new DruidDataSource();
        dataSource2.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource2.setUrl("jdbc:mysql://127.0.0.1:3306/ds1");
        dataSource2.setUsername("root");
        dataSource2.setPassword("root");
        dataSourceMap.put("ds1", dataSource2);

        // 配置Order表规则："ds${0..1}.t_order"表示对t_order分库ds0、ds1
        TableRuleConfiguration orderTableRuleConfig = new TableRuleConfiguration("t_order", "ds${0..1}.t_order");
        // 分库策略，使用inline实现："ds${order_id % 2}"表示分库采用《取模》策略。${order_id % 2}表达式等于0或者等于1，与ds组合起来，即是ds0或者ds1
        InlineShardingStrategyConfiguration dataBaseInlineStrategy = new InlineShardingStrategyConfiguration("order_id", "ds${order_id % 2}");
        orderTableRuleConfig.setDatabaseShardingStrategyConfig(dataBaseInlineStrategy);
        // 分表策略，使用inline实现（没有分表，为什么不分表？）："t_order"表示不分表
        InlineShardingStrategyConfiguration tableInlineStrategy = new InlineShardingStrategyConfiguration("order_id", "t_order");
        orderTableRuleConfig.setTableShardingStrategyConfig(tableInlineStrategy);

        // 添加表配置
        ShardingRuleConfiguration shardingRuleConfig = new ShardingRuleConfiguration();
        shardingRuleConfig.getTableRuleConfigs().add(orderTableRuleConfig);

        // 获取数据源对象
        DataSource dataSource = ShardingDataSourceFactory.createDataSource(dataSourceMap, shardingRuleConfig, new Properties());

        //查询测试
        String sql = "SELECT * from t_order WHERE order_id =?";
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setLong(1, 3);
            System.out.println();
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    // %2结果，路由到
                    System.out.println("---------order_id：" + rs.getLong(1));
                    System.out.println("---------user_id：" + rs.getLong(2));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        //插入测试
        /*String sql = "insert into t_order values (?, ?)";
        try {
            Connection conn = dataSource.getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            for(int i = 0; i<10; i++){
                preparedStatement.setInt(1, i);
                preparedStatement.setInt(2, i);
                boolean execute = preparedStatement.execute();
                System.out.println("insert 情况：" + execute);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }
}