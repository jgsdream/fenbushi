package com.qingshan;

import com.qingshan.service.OrderItemService;
import com.qingshan.service.OrderService;
import com.qingshan.service.UserInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShardTableTest {

    @Autowired
    private UserInfoService userInfoService;

    @Resource
    private OrderItemService orderItemService;

    @Autowired
    private OrderService orderService;

    @Test
    public void insertUserInfoDemo() {
        // 插入10条用户数据
        userInfoService.insertBatchUserInfo();
    }

    @Test
    public void queryUserInfo() {
        // 读写分离，发送到 slave1 查询 user_info
        //userInfoService.queryUserIfo(1L);
        // 读写分离，发送到 slave0 查询 user_info
         userInfoService.queryUserIfo(2L);
    }


    @Test
    public void insertOrder() {
        // 插入10条 order数据
        orderService.insertBatchOrder();
    }

    @Test
    public void queryOrderById() {
        // 读写分离，发送到 slave1 查询 t_order
        // orderService.queryOrderById(1L);
        // 读写分离，发送到 slave0 查询 t_order
        orderService.queryOrderById(2L);
    }


    @Test
    public void insertOrderItem() {
        // 插入10条 orderItem数据
        orderItemService.insertBatchOrderItem();
    }

    @Test
    public void queryOrderItemById() {
        // 读写分离，发送到 slave1 查询 t_order_item
        // orderInfoService.queryOrderById(1L);
        // 读写分离，发送到 slave0 查询 t_order_item
        orderItemService.queryOrderItemById(2L);
    }

}
